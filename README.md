# toto-sammy

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Installation vue js
```
    npm install -g vue etc...
``` 

### creation du projet vue js
```
    vue create nom_project
``` 

### ajouter vuetify au projet vue js sans oublié  l'installation router-view 
```
    vue create nom_project
    npm install etc...
``` 

###  l'installation material-app 
```
    npm install etc...
``` 