import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify'
import Chartkick from 'vue-chartkick'
import Chart from 'chart.js'
import VueCharts from 'vue-chartjs'
//import { Bar, Line } from 'vue-chartjs'
//import VueMaterial from 'vue-material'
//import 'vue-material/dist/vue-material.min.css'
//import 'vue-material/dist/theme/default.css'

Vue.use(VueCharts)
Vue.use(Chartkick.use(Chart))
Vue.config.productionTip = false

new Vue({
  vuetify,
  router,
  VueCharts,
  render: h => h(App)
}).$mount('#app')
